Seabreyh Resource Pack - Server Edition

 -x-

The Seabreyh Pack - A Server Resource Pack for Minecraft.

This Resourcepack is a project of the Seabreyh and is not affiliated with Mojang AB.

 -x-



--- Credits ---

Sounds:
	www.freesound.org

Textures:
	Textures Created by Brendan Weirtz
	www.weirtz.com

Music:
	Null

Font:
	Bariol-Regular
	www.bariol.com
	
	Font generated with HD Font Generator-1.2
	http://mnm-mods.blogspot.com/2015/03/hd-font-generator.html